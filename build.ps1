# Clean build directory
if (Test-Path "$PSScriptRoot\build") 
{ 
    Write-Host "Cleaning..."
    rm "$PSScriptRoot\build" -Recurse
} 

# Restore packages
Write-Host "Restoring ..."
pushd $PSScriptRoot
dotnet restore --disable-parallel
if($LASTEXITCODE -ne 0) { throw "Failed to restore" }
popd

# Publish to build directory
Write-Host "Building..."
dotnet publish "$PSScriptRoot\src" -o "$PSScriptRoot\build" --framework net45
if($LASTEXITCODE -ne 0) { throw "Failed to compile" }