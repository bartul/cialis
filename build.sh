set -e

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ "$SOURCE" != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"


 
if [ -d "$DIR/build/" ]; then
    echo "Cleaning..."
    rm -rf $DIR/build 
fi

# Restore packages
echo "Restoring..."
(
    cd $DIR
    dotnet restore --disable-parallel
)

# Publish to build directory
echo "Building..."
dotnet publish "$DIR/src" -o "$DIR/build" --framework net45

exit $?