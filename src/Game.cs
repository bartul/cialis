using System;
using System.Threading.Tasks;
using System.Text;
using System.Linq;
using System.Collections.Generic;

using Akka;
using Akka.Actor;

namespace Cialis
{

    class Game : TypedActor, IHandle<MakePlaceForNewTeam>, IHandle<TeamAtempt>, IHandle<ShowStatus>
    {
        private readonly Dictionary<string, IActorRef> teams = new Dictionary<string, IActorRef>();

        public void Handle(MakePlaceForNewTeam command)
        {
            if(teams.ContainsKey(command.TeamName)) { return; }

            var newTeam = Context.ActorOf<TeamProgress>(command.TeamName);
            teams.Add(command.TeamName, newTeam);
            
        }
        public void Handle(TeamAtempt atempt)
        {
            teams[atempt.TeamName].Tell(new Atempt(atempt.Answer));
        }        

        public void Handle(ShowStatus command)
        {
            var builder = new StringBuilder();
            builder.AppendLine("==============================================");
            foreach (KeyValuePair<string, IActorRef> item in teams)
            {
                builder.AppendLine($"## Team '{item.Key}' ##");
                builder.Append(item.Value.Ask(new ShowStatus()).Result);
            }
            Sender.Tell(builder.ToString(), Self);
            builder.AppendLine("=============================================="); 
        }
    }
    class TeamProgress : TypedActor, IHandle<Atempt>, IHandle<ShowStatus>
    {
        private IActorRef stage1 = Context.ActorOf(SolvableProblem.CreateProps(new UltimateQuestion()));
        private IActorRef stage2 = Context.ActorOf(SolvableProblem.CreateProps(new Torcida()));

        public void Handle(Atempt atempt)
        {
            if(!(bool)stage1.Ask(new {}).Result) { stage1.Tell(atempt.Answer); }
            else if(!(bool)stage2.Ask(new {}).Result) { stage2.Tell(atempt.Answer); }
        }        
        public void Handle(ShowStatus command)
        {
            var builder = new StringBuilder();
            builder.AppendLine(": Team Progress :");
    
            var t1 = stage1.Ask(new ShowStatus());
            var t2 = stage2.Ask(new ShowStatus());

            Task.WaitAll(t1, t2);

            builder.AppendLine(t1.Result.ToString());
            builder.AppendLine(t2.Result.ToString());
            
            Sender.Tell(builder.ToString(), Self);
        }
    }

    
    class MakePlaceForNewTeam 
    {
        public MakePlaceForNewTeam(string teamName)
        {
            TeamName = teamName;
        }
        public string TeamName { get; }
    }
    class TeamAtempt
    {
        public TeamAtempt(string teamName, object answer) 
        {
            TeamName = teamName;
            Answer = answer;
        }
        public string TeamName { get; }
        public object Answer { get; }
    }
    class Atempt
    {
        public Atempt(object answer) 
        {
            Answer = answer;
        }
        public object Answer { get; }
    }    
    class ShowStatus 
    {
    }
}