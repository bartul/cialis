using System;
using System.Text;
using System.Linq;
using System.Collections.Generic;

using Akka;
using Akka.Actor;

namespace Cialis
{
    static class SolvableProblem
    {
        public static Props CreateProps<T>(ISolvable<T> problem)
        {
            return Props.Create(() => new SolvableProblem<T>(problem));
        }
    }
    class SolvableProblem<T> : ReceiveActor 
    {
        private ISolvable<T> _problem;
        public SolvableProblem(ISolvable<T> problem)
        {
            _problem = problem;
            Become(Unsolved);
        }        
        
        void Solved()
        {
            Receive<ShowStatus>(msg => { Sender.Tell($"{_problem.Description} > Solved", Self); });
            ReceiveAny(msg => Sender.Tell(true, Self));       
        }
        void Unsolved()
        {
            Receive<T>(msg => {
                if(_problem.TrySolve(msg))
                {
                    Become(Solved);
                    Sender.Tell(true, Self);   
                }
                else 
                {
                    Sender.Tell(false, Self);
                }
            });
            Receive<ShowStatus>(msg => { Sender.Tell($"{_problem.Description} > Unsolved", Self); });
            ReceiveAny(msg => Sender.Tell(false, Self));       
        }
    }
}