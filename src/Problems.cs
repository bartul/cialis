using System;
using System.Collections.Generic;

using Akka;
using Akka.Actor;

namespace Cialis
{
    interface ISolvable<T>
    {
        bool TrySolve(T answer);
        string Description { get; } 
    }
    class UltimateQuestion : ISolvable<string>
    {
        public bool TrySolve(string answer) => (answer == "42");
        public string Description => "What is the meaning of life?";
    }
    class Torcida : ISolvable<int>
    {
        public bool TrySolve(int answer) => (answer == 1950);
        public string Description => "When did the oldest europian football supporter group formed?";
    }
}