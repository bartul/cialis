﻿using System;

using Akka;
using Akka.Actor;
 
namespace Cialis
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var system = ActorSystem.Create("cialis");

            var game = system.ActorOf<Game>("game");
            game.Tell(new MakePlaceForNewTeam("Alfa"));
            game.Tell(new MakePlaceForNewTeam("Beta"));
            Console.WriteLine(game.Ask(new ShowStatus()).Result);

            game.Tell(new TeamAtempt("Alfa", "Children"));
            game.Tell(new TeamAtempt("Beta", "42"));
            Console.WriteLine(game.Ask(new ShowStatus()).Result);
            
            game.Tell(new TeamAtempt("Alfa", "Love"));
            game.Tell(new TeamAtempt("Beta", 1950));
            Console.WriteLine(game.Ask(new ShowStatus()).Result);
            
            Console.Write(">> PRESS ANY KEY <<");
            Console.Read();
        }
    }
}
